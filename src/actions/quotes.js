import axios from 'axios';
import {GET_QUOTE_BY_CATEGORY, QUOTE_ERROR, RESET_QUOTE} from "../actionTypes";

export const getQuoteByCategory = (category) =>
    axios.get(`https://quotes.rest/qod?category=${category}`).then(res => {
        return {
            type: GET_QUOTE_BY_CATEGORY,
            quote: res.data.contents.quotes[0]
        }
    }).catch(err => {
        console.log(err);
        return {
            type: QUOTE_ERROR,
            quote: {
                quote: 'Sorry, there were some incomprehensible server errors.',
                author: 'Developer'
            }
        }
    });

export const resetQuote = () => ({
    type: RESET_QUOTE
});