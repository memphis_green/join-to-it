import axios from 'axios';
import {CATEGORY_ERROR, GET_CATEGORIES, SET_OFFSET} from "../actionTypes";

export const getCategories = () =>
    axios.get('https://quotes.rest/qod/categories').then(res => {
            return ({
                type: GET_CATEGORIES,
                categories: res.data.contents.categories
            })
        }
    ).catch(err => {
        console.log(err);
        return {
            type: CATEGORY_ERROR,
            error: {
                quote: 'Sorry, there were some incomprehensible server errors.',
                author: 'Developer'
            }
        }
    });

export const setOffset = (offset) => ({
    type: SET_OFFSET,
    offset
});
