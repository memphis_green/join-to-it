import {GET_QUOTE_BY_CATEGORY, QUOTE_ERROR, RESET_QUOTE} from "../actionTypes";

const initialState = {
    quote: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_QUOTE_BY_CATEGORY:
            return Object.assign({}, state, {quote: action.quote});
        case RESET_QUOTE:
            return Object.assign({}, state, {quote: ''});
        case QUOTE_ERROR:
            return Object.assign({}, state, {quote: action.quote});
        default:
            return state;
    }
}