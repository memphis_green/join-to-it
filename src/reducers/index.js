import { combineReducers } from 'redux';
import categories from './categories';
import quotes from './quotes';

const reducers = combineReducers({
    categories,
    quotes
});

export default reducers;