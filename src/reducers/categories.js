import {CATEGORY_ERROR, GET_CATEGORIES, SET_OFFSET} from "../actionTypes";

const initialState = {
    offset: 0,
    list: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES:
            return Object.assign({}, state, {list: Object.entries(action.categories).map(([name, desc]) => ({name, desc}))});
        case CATEGORY_ERROR:
            return Object.assign({}, state, {error: action.error});
        case SET_OFFSET:
            return Object.assign({}, state, {offset: action.offset});
        default:
            return state;
    }
}