import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import { Link } from 'react-router-dom'

const Category = props => (
    <Link to={`/quote/${props.name}`} className={'Category'}
          style={{'maxWidth': `${props.maxWidth}px`}}
    >
        <img src={props.src} alt={props.name}/>
        <p>{props.desc}</p>
    </Link>
);

Category.propTypes = {
    src: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    maxWidth: PropTypes.number.isRequired
};

export default Category;