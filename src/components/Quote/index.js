import React from 'react';
import PropTypes from 'prop-types';
import './style.scss'

export const Quote = props => (
    <div className={'Quote--page__card'}>
        <h2>{props.quote}</h2>
        <h3><i>&#8212; {props.author}</i></h3>
        <button onClick={() => props.newQuote()}>New quote</button>
    </div>
);

Quote.propTypes = {
    newQuote: PropTypes.func.isRequired,
    quote: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
};

export default Quote;