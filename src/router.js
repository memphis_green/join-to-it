import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import Categories from './containers/Categories';
import Quote from './containers/Quote';

export default () => (
    <Router>
        <Switch>
            <Route exact path={'/'} render={() => <Redirect to={'/categories'}/>}/>
            <Route exact path={'/categories'} component={Categories}/>
            <Route exact path={'/quote/:category'} component={Quote}/>
        </Switch>
    </Router>
);