import React from 'react';
import {connect} from 'react-redux';
import {getQuoteByCategory, resetQuote} from "../../actions/quotes";
import Card from '../../components/Quote';
import BackArrow from '../../components/BackArrow';
import propTypes from 'prop-types';

class Quote extends React.Component {
    componentDidMount() {
        this.category = this.props.match.params.category;
        this.props.getQuoteByCategory(this.category);
    }

    getNewQuote = () => {
        this.props.getQuoteByCategory(this.category);
    };

    componentWillUnmount() {
        this.props.resetQuote();
    }

    render() {
        const {quote} = this.props;
        return (
            <section className={'Quote--page'}>
                <BackArrow url={'/categories'}/>
                {quote
                    ? <Card author={quote.author || 'Unknown author'} quote={quote.quote} newQuote={() => this.getNewQuote()}/>
                    : null
                }
            </section>
        )
    }
}

Quote.propTypes = {
    quote: propTypes.object.isRequired,
    getQuoteByCategory: propTypes.func.isRequired,
    resetQuote: propTypes.func.isRequired
};

export default connect(
    state => ({
        quote: state.quotes.quote
    }),
    dispatch => ({
        getQuoteByCategory: async (category) => dispatch(await getQuoteByCategory(category)),
        resetQuote: async () => dispatch(await resetQuote())
    })
)(Quote);