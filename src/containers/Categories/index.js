import React from 'react';
import {connect} from 'react-redux';
import {getCategories, setOffset} from "../../actions/categories";
import Category from '../../components/Category';
import {getImageByCategory} from "../../helpers";
import propTypes from 'prop-types';

class Categories extends React.Component {
    state = {
        windowWidth: document.documentElement.clientWidth
    };

    componentDidMount() {
        window.addEventListener('resize', this.resizeListener);
        this.props.getCategories();
        this.setOffset();
    }

    setOffset = () => window.scrollTo(0, this.props.offset);

    resizeListener = () => {
        this.setState({
            windowWidth: this.getWindowWidth()
        })
    };

    getWindowWidth = () => document.documentElement.clientWidth;

    getColumnMaxWidth = () => this.state.windowWidth <= 400 ? this.state.windowWidth : 400;

    getColumnsNumber = () => Math.floor(this.state.windowWidth / this.getColumnMaxWidth());

    sortCategories = () => Array.apply(null, Array(this.getColumnsNumber())).map((value, index) => this.fillColumn(index + 1));

    fillColumn = index => this.props.list.filter((category, _index) =>
        (_index < index * this.getColumnMaxLength()) && _index >= this.getColumnMaxLength() * (index - 1)).reverse();

    getColumnMaxLength = () => Math.ceil(this.props.list.length / this.getColumnsNumber());

    componentWillUnmount() {
        window.removeEventListener('resize', this.resizeListener);
        this.props.setOffset(window.pageYOffset);
    }

    render() {
        const {list, error} = this.props;

        if (error) {
            return ( <div className={'Categories--page'}>
                <p>Server error, please reload this page</p>
            </div>)
        }


        if (list.length) {
            return (
                <div className={'Categories--page'}>
                    {this.sortCategories().map((column, index) => (
                        <div className={'Categories--page__column'}
                             key={index}
                        >
                            {column.map((category, _index) =>
                                <Category key={_index}
                                          src={getImageByCategory(category.name)}
                                          name={category.name}
                                          desc={category.desc}
                                          maxWidth={this.getColumnMaxWidth()}
                                />)}
                        </div>
                    ))}
                </div>
            )
        }
        return null;
    }
}

Categories.propTypes = {
    getCategories: propTypes.func.isRequired,
    setOffset: propTypes.func.isRequired,
    list: propTypes.array.isRequired,
    offset: propTypes.number.isRequired,
    error: propTypes.object
};

export default connect(
    state => ({
        list: state.categories.list,
        offset: state.categories.offset,
        error: state.categories.error
    }),
    dispatch => ({
        getCategories: async () => dispatch(await getCategories()),
        setOffset: async (offset) => dispatch(await setOffset(offset))
    }))(Categories);