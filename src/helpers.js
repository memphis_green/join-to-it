/* images */
import inspire from './images/inspire.jpg';
import management from './images/management.jpg';
import sports from './images/sports.png';
import life from './images/life.jpg';
import funny from './images/funny.jpg';
import love from './images/love.jpg';
import art from './images/art.jpg';
import students from './images/students.jpg';

const album = {inspire, management, sports, life, funny, love, art, students};

export const getImageByCategory = (category) => album[category];