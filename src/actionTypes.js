/* Categories */
export const GET_CATEGORIES = 'GET_CATEGORIES';
export const SET_OFFSET = 'SET_OFFSET';

/* Quotes */
export const GET_QUOTE_BY_CATEGORY = 'GET_QUOTE_BY_CATEGORY';
export const RESET_QUOTE = 'RESET_QUOTE';

/* Errors */
export const QUOTE_ERROR = 'QUOTE_ERROR';
export const CATEGORY_ERROR = 'CATEGORY_ERROR';