import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducers from './reducers';
import './reset.scss';

const store = createStore(reducers);

ReactDOM.render(
    <Provider store={store}>
        <Router/>
    </Provider>, document.getElementById('root'));

